<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->enum('type', ['AMOUNT', 'PERCENT'])->default('PERCENT');
            $table->integer('value')->default(0);
            $table->enum('valid_in', ['DELIVERY', 'ONSITE_SERVICE', 'BOTH'])->default('BOTH');
            $table->timestamp('valid_from');
            $table->timestamp('valid_until');
            $table->integer('qty_max')->nullable();
            $table->string('selected_companies')->nullable();
            $table->string('selected_users')->nullable();
            $table->string('selected_products')->nullable();
            $table->enum('status', ['ON', 'OFF'])->default('OFF');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
