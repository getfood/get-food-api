<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductExtraFieldAssocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_extra_field_assocs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('products_extra_fields_id');
            $table->foreign('products_extra_fields_id')->references('id')->on('product_extra_fields')->onDelete('cascade');
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->enum('type', ['CHANGE_PRICE', 'ADD_PRICE', 'NONE'])->default('NONE');
            $table->bigInteger('value')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_extra_field_assocs');
    }
}
