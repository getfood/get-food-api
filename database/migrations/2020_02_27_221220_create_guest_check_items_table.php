<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuestCheckItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_check_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('guest_check_id');
            $table->foreign('guest_check_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('price')->default(0);
            $table->enum('status', ['WAITING', 'READY', 'DELIVERED', 'PAID', 'CANCELED'])->default('WAITING');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_check_items');
    }
}
