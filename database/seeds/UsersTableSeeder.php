<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdmin();
        $this->createOwner();
        $this->createEmployee();
        $this->createClient();
    }

    private function createAdmin () {
        $admin              = new User();
        $admin->name        = "Sir Admin";
        $admin->email       = "sir@admin";
        $admin->password    = bcrypt("123");
        $admin->role        = "ADMIN";
        $admin->save();
    }

    private function createOwner () {
        $owner              = new User();
        $owner->name        = "Sir Owner";
        $owner->email       = "sir@owner";
        $owner->password    = bcrypt("123");
        $owner->role        = "OWNER";
        $owner->save();
    }

    private function createEmployee () {
        $employee           = new User();
        $employee->name     = "Sir Employee";
        $employee->email    = "sir@emplyee";
        $employee->password = bcrypt("123");
        $employee->role     = "EMPLOYEE";
        $employee->save();
    }

    private function createClient () {
        $client             = new User();
        $client->name       = "Sir Client";
        $client->email      = "sir@client";
        $client->password   = bcrypt("123");
        $client->role       = "CLIENT";
        $client->save();
    }
}
