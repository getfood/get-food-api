<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'id',
        'company_id',
        'type',
        'status',
    ];

    public function produtos () {
        return $this->hasMany('App\Product', 'menu_id', 'id');
    }
}
