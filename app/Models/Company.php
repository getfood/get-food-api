<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'id',
        'img',
        'thumb',
        'name',
        'company_name',
        'address_id',
        'owner_id',
        'status',
    ];

    public function owner () {
        return $this->hasOne('App\User', 'id', 'owner_id');
    }

    public function contract () {
        $this->hasOne('App\Contract', 'company_id', 'id');
    }

    public function employeers () {
        return $this->hasMany('App\Employee', 'company_id', 'id');
    }

    public function menus () {
        return $this->hasMany('App\Menu', 'company_id', 'id');
    }

    public function address () {
        return $this->hasOne('App\Address', 'relationship_id', 'id');
    }

    public function tables () {
        return $this->hasMany('App\RestaurantTable', 'company_id', 'id');
    }

    public function guest_checks () {
        return $this->hasMany('App\GuestCheck', 'company_id', 'id');
    }
}
