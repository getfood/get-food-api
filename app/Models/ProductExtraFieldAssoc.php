<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductExtraFieldAssoc extends Model
{
    protected $fillable = [
        'id',
        'products_extra_fields_id',
        'product_id',
        'type',
        'value'
    ];
}
