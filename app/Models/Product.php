<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'id',
        'img',
        'thumb',
        'company_id',
        'category_id',
        'name',
        'description',
        'price',
        'show_in_menu',
        'status'
    ];

    public function company () {
        return $this->hasOne('App\Company', 'id', 'company_id');
    }

    public function category () {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function additionals () {
        return $this->hasMany('App\ProductExtraField', 'product_id', 'id');
    }
}
