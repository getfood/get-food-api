<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'id',
        'company_id',
        'menu_id',
        'slug',
        'name',
        'status'
    ];
}
