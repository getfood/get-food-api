<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductExtraField extends Model
{
    protected $fillable = [
        'id',
        'product_id',
        'title',
        'qty_min',
        'qty_max'
    ];
}
