<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuestCheckItem extends Model
{
    protected $fillable = [
        'id',
        'guest_check_id',
        'product_id',
        'price',
        'status'
    ];

    public function product () {
        return $this->hasOne('App\Product', 'id', 'company_id');
    }
}
