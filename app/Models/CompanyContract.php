<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyContract extends Model
{
    protected $fillable = [
        'id',
        'company_id',
        'delivery_payment_type',
        'delivery_payment_value',
        'onsite_service_payment_type',
        'onsite_service_payment_value',
    ];
}
