<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuestCheck extends Model
{
    protected $fillable = [
        'id',
        'company_id',
        'table_id',
        'value_to_pay',
        'closed_at',
        'paid_at',
        'status'
    ];

    public function items () {
        return $this->hasMany('App\GuestCheckItem', 'guest_check_id', 'id');
    }
}
