<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantTable extends Model
{
    protected $fillable = [
        'id',
        'company_id',
        'local_id',
        'status',
    ];
}
