<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'id',
        'relationship_id',
        'relationship_type',
        'city',
        'district',
        'street',
        'number_letter',
        'complement',
        'zipcode',
        'status',
    ];
}
