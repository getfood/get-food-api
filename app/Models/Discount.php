<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = [
        'id',
        'company_id',
        'type',
        'value',
        'valid_in',
        'valid_from',
        'valid_until',
        'qty_max',
        'selected_companies',
        'selected_users',
        'selected_products',
        'status'
    ];
}
