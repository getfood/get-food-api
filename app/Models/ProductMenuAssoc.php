<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductMenuAssoc extends Model
{
    protected $fillable = [
        'id',
        'product_id',
        'menu_id'
    ];
}
